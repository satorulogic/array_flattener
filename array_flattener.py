def flatten(list_):
    return list(iflatten(list_))


def iflatten(list_, seen=None):
    if seen is None:
        seen = set()
    if id(list_) in seen:
        raise TypeError
    else:
        seen.add(id(list_))

    for i in list_:
        if isinstance(i, list):
            yield from iflatten(i, seen)
        else:
            yield i


if __name__ == '__main__':
    print(flatten([[1, 2, [3]], [4, [5]]]))
