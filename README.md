Array Flattener
===

## Alternative solution?

Actually, I have hidden another solution that's not recursion based.

```bash
$ git tag -l
deque-solution
recursion-solution
```
So you can use `git co deque-solution` to have a look at that solution, and use `git co -` to return.

## How to run the script?

If you have Python 3.5+ installed on your machine, just run `python array_flattener.py`.

Otherwise, you may want to try it with [this Github gist](https://gist.github.com/suzaku/46748559ac0df37f3bc6b5998e0585f5) and the [BitRun](https://chrome.google.com/webstore/detail/eaaciohkbnfefblneeiffopbakjaaodk) Chrome exntension.

It seems like BitRun uses Python 2 by default, so the second script in the given gist won't run successfully this way, because it uses a Python 3 only syntax: `yield from`.

## How to run the tests?

The tests are written in the `py.test` style, which is pretty simple.

So if you want to run the tests, you need to install `pytest`.

```bash
pip install pytest
```

Then you can just run `py.test test_array_flattener.py`.
