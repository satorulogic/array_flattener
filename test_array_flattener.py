from unittest import TestCase

from array_flattener import flatten


class TestFlatten(TestCase):

    def test_flattened_list_should_stay_flattened(self):
        assert flatten([]) == []
        assert flatten([2, 0, 1, 6]) == [2, 0, 1, 6]

    def test_should_flatten_nested_lists(self):
        assert flatten([[1, 2, [3]], 4]) == [1, 2, 3, 4]
        assert flatten([[[[[42]]], 1], [1]]) == [42, 1, 1]

    def test_should_raise_error_on_circular_reference(self):
        x = []
        x.append(x)
        x.append(42)
        with self.assertRaises(TypeError):
            flatten(x)
